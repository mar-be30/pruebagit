﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CambiarPantallas : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        

    }

    public void openEscenPerfil()
    {
        SceneManager.LoadScene("Perfil");
    }

    public void openEscenCartModul()
    {
        SceneManager.LoadScene("CartaModulos");
    }


    public void openEscenModulUn()
    {
        SceneManager.LoadScene("Modulos");
    }
    public void openEscenMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void openEscenJuegoU()
    {
        SceneManager.LoadScene("JuegoUn");
    }
    public void openEscenJuego2()
    {
        SceneManager.LoadScene("Modulos");
    }
    public void openEscenJuego3()
    {
        SceneManager.LoadScene("Modulos");
    }
    public void openEscenInsignias()
    {
        SceneManager.LoadScene("Insignias");
    }

    public void cerrarAplicacion()
    {
        Application.Quit();
        
    }

}
