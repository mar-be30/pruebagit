﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class ControlRespuestasJU : MonoBehaviour
{
    public Toggle opcion1;
    public Toggle opcion2;
    public Toggle opcion3;
    public Toggle opcion4;
    public Toggle opcion5;
    public Toggle opcion6;
    public Toggle opcion7;
    public Toggle opcionRemover;

    public GameObject panelVerificacionCorrecto;
    public GameObject panelVerificacionIncorrecto;

    public int index;
    private List<Toggle> selecToggle;
    public string[] correctas;
    List<string> seleccionadas = new List<string>();
    private JSONNode dataOpciones;
    private JSONNode data;
    private DatosPregunta preguntaSel;
    public string[] todo;
    public string[] incorrectas;

    public GameObject moduloPregunta;


    public Text textoTitulo;
    public Text textoSubTitulo;


    public Text textoOpcion1;
    public Text textoOpcion2;
    public Text textoOpcion3;
    public Text textoOpcion4;


    public string opcionpalabra1;
    public string opcionpalabra2;
    public string opcionpalabra3;
    public string opcionpalabra4;

    // Start is called before the first frame update
    void Start()
    {
        opcion1.isOn = false;
        opcion2.isOn = false;
        opcion3.isOn = false;
        opcion4.isOn = false;
        // opcion5.isOn = false;
        // opcion6.isOn = false;
        // opcion7.isOn = false;
        selecToggle = new List<Toggle>();
        selecToggle.Add(opcion1);
        index = 1;
        panelVerificacionIncorrecto.SetActive(false);
        panelVerificacionCorrecto.SetActive(false);


        //Debug.Log((modulo.GetComponent<DatosModuloElegido>().idModulo).text);
        preguntaSel = moduloPregunta.GetComponent<DatosPregunta>();
        var i = preguntaSel.opciones.Count;
        dataOpciones = preguntaSel.opciones;


        textoTitulo.text = preguntaSel.descripcion;

        textoSubTitulo.text = preguntaSel.nombre;

        var limiteOpciones = 4; // todo from pregunta objeto

        if (i > 3)
        {
            todo = new string[limiteOpciones];
        }
        else
        {
            todo = new string[i];
        }

        var j = 0;
        var k = 0;


        foreach (JSONNode opciones in dataOpciones)
        {
            if (opciones["valida"] == true)
            {
                j++;
            }
            else
            {
                k++;
            }
        }

        correctas = new string[j];
        incorrectas = new string[k];

        j = 0;
        k = 0;
        var m = 0;

        foreach (JSONNode opciones in dataOpciones)
        {
            if (opciones["valida"] == true)
            {
                correctas[j] = opciones["text"];
                j++;
                m++;
            }
            else
            {
                incorrectas[k] = opciones["text"];
                k++;
                m++;
            }
        }

        for (int t = 0; t < incorrectas.Length; t++)
        {
            string tmp = incorrectas[t];
            int r = Random.Range(t, incorrectas.Length-1);
            incorrectas[t] = incorrectas[r];
            incorrectas[r] = tmp;
        }

        int d = 0;
        int e = 0;

        for (int t = j; t < todo.Length; t++)
        {
            if (t == j)
            {
                for (int p = 0; p < correctas.Length; p++)
                {
                    todo[p] = correctas[e];
                    e++;
                }

                todo[t] = incorrectas[d];
                d++;
            }
            else
            {
                todo[t] = incorrectas[d];
                d++;
            }
        }


        for (int t = 0; t < todo.Length; t++)
        {
            string tmp = todo[t];
            int r = Random.Range(t, todo.Length-1);
            todo[t] = todo[r];
            todo[r] = tmp;
        }




        switch (limiteOpciones)
        {
            case 2:
                opcion1.gameObject.SetActive(true);
                textoOpcion1.text = todo[0];
                opcion2.gameObject.SetActive(true);
                textoOpcion2.text = todo[1];
                opcion3.gameObject.SetActive(false);
                opcion4.gameObject.SetActive(false);
                opcion5.gameObject.SetActive(false);
                opcion6.gameObject.SetActive(false);
                opcion7.gameObject.SetActive(false);
                break;
            case 3:
                opcion1.gameObject.SetActive(true);
                textoOpcion1.text = todo[0];
                opcion2.gameObject.SetActive(true);
                textoOpcion2.text = todo[1];
                opcion3.gameObject.SetActive(true);
                textoOpcion3.text = todo[2];
                opcion4.gameObject.SetActive(false);
                opcion5.gameObject.SetActive(false);
                opcion6.gameObject.SetActive(false);
                opcion7.gameObject.SetActive(false);
                break;
            case 4:
                opcion1.gameObject.SetActive(true);
                textoOpcion1.text = todo[0];
                opcion2.gameObject.SetActive(true);
                textoOpcion2.text = todo[1];
                opcion3.gameObject.SetActive(true);
                textoOpcion3.text = todo[2];
                opcion4.gameObject.SetActive(true);
                textoOpcion4.text = todo[3];
                opcion5.gameObject.SetActive(false);
                opcion6.gameObject.SetActive(false);
                opcion7.gameObject.SetActive(false);
                break;
            case 5:
                opcion1.gameObject.SetActive(true);
                //    texto1.text = nombresUni[0];
                opcion2.gameObject.SetActive(true);
                //    texto2.text = nombresUni[1];
                opcion3.gameObject.SetActive(true);
                //    texto3.text = nombresUni[2];
                opcion4.gameObject.SetActive(true);
                //   texto4.text = nombresUni[3];
                opcion5.gameObject.SetActive(true);
                //  texto5.text = nombresUni[4];
                opcion6.gameObject.SetActive(false);
                opcion7.gameObject.SetActive(false);
                break;
            case 6:
                opcion1.gameObject.SetActive(true);
                //    texto1.text = nombresUni[0];
                opcion2.gameObject.SetActive(true);
                //    texto2.text = nombresUni[1];
                opcion3.gameObject.SetActive(true);
                //    texto3.text = nombresUni[2];
                opcion4.gameObject.SetActive(true);
                //     texto4.text = nombresUni[3];
                opcion5.gameObject.SetActive(true);
                //    texto5.text = nombresUni[4];
                opcion6.gameObject.SetActive(true);
                //     texto6.text = nombresUni[5];
                opcion7.gameObject.SetActive(false);
                break;
            case 7:
                opcion1.gameObject.SetActive(true);
                //    texto1.text = nombresUni[0];
                opcion2.gameObject.SetActive(true);
                //    texto2.text = nombresUni[1];
                opcion3.gameObject.SetActive(true);
                //    texto3.text = nombresUni[2];
                opcion4.gameObject.SetActive(true);
                //   texto4.text = nombresUni[3];
                opcion5.gameObject.SetActive(true);
                //   texto5.text = nombresUni[4];
                opcion6.gameObject.SetActive(true);
                //   texto6.text = nombresUni[5];
                opcion7.gameObject.SetActive(true);
                //   texto7.text = nombresUni[6];
                break;
            default:
                // code block
                break;
        }
    }

    void OnGUI()
    {
        // Make a multiline text area that modifies stringToEdit.
        //stringToEdit = GUI.TextArea(new Rect(10, 10, 200, 100), stringToEdit, 200);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void agregarOpcion1()
    {
        // seleccionadas = new string[2];

        if (opcion1.isOn == true)
        {
            selecToggle.Add(opcion1);
            opcionpalabra1 = textoOpcion1.GetComponent<Text>().text;
        }
        else
        {
            selecToggle.Remove(opcion1);
            opcionpalabra1 = "";
        }

        numOpciones();
    }

    public void agregarOpcion2()
    {
        if (opcion2.isOn == true)
        {
            selecToggle.Add(opcion2);
            opcionpalabra2 = textoOpcion2.GetComponent<Text>().text;
        }
        else
        {
            selecToggle.Remove(opcion2);
            opcionpalabra2 = "";
        }

        numOpciones();
    }

    public void agregarOpcion3()
    {
        if (opcion3.isOn == true)
        {
            selecToggle.Add(opcion3);
            opcionpalabra3 = textoOpcion3.GetComponent<Text>().text;
        }
        else
        {
            selecToggle.Remove(opcion3);
            opcionpalabra3 = "";
        }

        numOpciones();
    }

    public void agregarOpcion4()
    {
        if (opcion4.isOn == true)
        {
            selecToggle.Add(opcion4);
            opcionpalabra4 = textoOpcion4.GetComponent<Text>().text;
        }
        else
        {
            selecToggle.Remove(opcion4);
            opcionpalabra4 = "";
        }

        numOpciones();
    }

    public void agregarOpcion5()
    {
        if (opcion5.isOn == true)
        {
            selecToggle.Add(opcion5);
        }
        else
        {
            selecToggle.Remove(opcion5);
        }
    }

    public void agregarOpcion6()
    {
        if (opcion6.isOn == true)
        {
            selecToggle.Add(opcion6);
        }
        else
        {
            selecToggle.Remove(opcion6);
        }
    }

    public void agregarOpcion7()
    {
        if (opcion7.isOn == true)
        {
            selecToggle.Add(opcion7);
        }
        else
        {
            selecToggle.Remove(opcion7);
        }
    }

    public void numOpciones()
    {
        for (int index = 0; index < selecToggle.Count; index++)
        {
            if (preguntaSel.multi)
            {
                if (selecToggle.Count > 2 && index > 2)
                {
                    Debug.Log("num index " + index);
                    int num = index - 2;
                    Debug.Log("num : " + num);
                    opcionRemover = selecToggle[num];
                    opcionRemover.isOn = false;
                    opcionRemover = null;
                    index = 1;
                }
            }
            else
            {
                if (selecToggle.Count > 1 && index > 1)
                {
                    Debug.Log("num index " + index);
                    int num = index - 1;
                    Debug.Log("num : " + num);
                    opcionRemover = selecToggle[num];
                    opcionRemover.isOn = false;
                    opcionRemover = null;
                    index = 1;
                }
            }
        }
    }

    public void validarResJU()
    {
        seleccionadas.Add(opcionpalabra1);
        seleccionadas.Add(opcionpalabra2);
        seleccionadas.Add(opcionpalabra3);
        seleccionadas.Add(opcionpalabra4);


        Debug.Log("Pasa a la siguiente pantalla ");

        int contador = 0;

        for (int i = 0; i < correctas.Length; i++)
        {
            for (int j = 0; j < seleccionadas.Count; j++)
            {
                if (correctas[i] == seleccionadas[j])
                {
                    contador++;
                }
            }
        }

        Debug.Log(contador);
        bool correct = false;
        if (preguntaSel.multi)
        {
            if (contador == 2)
            {
                correct = true;
                Debug.Log("Correcto");
            }

            if ((contador == 1))
            {
                correct = true;
                Debug.Log("casi ");
            }

            if (contador == 0)
            {
                correct = false;
                Debug.Log("no ");
            }
        }
        else
        {
            if ((contador == 1))
            {
                correct = true;
                Debug.Log("Correcto");
            }
        }


        if (correct)
        {
            panelVerificacionCorrecto.SetActive(true);
            panelVerificacionCorrecto.transform.Find("detail").GetComponent<Text>().text = preguntaSel.msgCorrecto;
        }
        else
        {
            panelVerificacionIncorrecto.SetActive(true);
            panelVerificacionIncorrecto.transform.Find("detail").GetComponent<Text>().text = preguntaSel.msgIncorrecto;
        }
    }
}