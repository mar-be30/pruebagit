﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsignarTexto : MonoBehaviour
{
    public Text txtCorrecConsejo;
    public Text txtCorrecConsejo2;
    public Text txtIncorreConsejo;
    public Text txtIncorreConsejo2;

    public string textoConsejoBien;
    public string textoConsejo2Bien;
    public string textoConsejoMal;
    public string textoConsejo2Mal;
    // Start is called before the first frame update
    void Start()
    {
        txtCorrecConsejo.text = textoConsejoBien;
        txtCorrecConsejo2.text = textoConsejo2Bien;
        txtIncorreConsejo.text = textoConsejoMal;
        txtIncorreConsejo2.text = textoConsejo2Mal;

    }

    // Update is called once per frame
    void Update()
    {
    }

    /*Sabes que los derechos son irrenunciables, todos los niños y 
     * niñas los tienen, nos protegen para que nadie abuse de nosotros 
     * y podamos vivir de manera tranquila y feliz. En cambio,
     * la responsabilidad es un deber que tienes que cumplir.*/

}
