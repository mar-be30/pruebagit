﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiPanlCrear : MonoBehaviour
{
    public GameObject panlPerfil;
    public GameObject panlCrear;
    // Start is called before the first frame update
    void Start()
    {
        panlPerfil.SetActive(true);
        panlCrear.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void buttonCrear()
    {
        panlCrear.SetActive(true);
        panlPerfil.SetActive(false);


    }
}
