﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatosTripulante 
{
    public string nombre;
    public Texture nombreImag;

    public DatosTripulante(string nom, Texture fot)
    {
        nombre = nom;
        nombreImag = fot;
    }
}
