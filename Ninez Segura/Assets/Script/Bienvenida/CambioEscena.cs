using System;
using System.Collections;
using System.IO;
using System.Net;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class CambioEscena : MonoBehaviour
{
    private int cont = 0;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(getPreguntas());
        StartCoroutine(getModulos());
        StartCoroutine(getUnidades());
        //imprimirElementos(); 
    }

    // Update is called once per frame
    void Update()
    {
        cont = (int) Time.time;

        if (cont >= 5)
        {
            cambioEscena();
        }
    }

    public void cambioEscena()
    {
        SceneManager.LoadScene("Ticket");
    }

    //public CargarElementos ce;
    // public static void imprimirElementos()
    // {
    //     Debug.Log("La escena se a iniciadoo");
    //     //ce = new CargarElementos();
    //     JSONNode data = CargarElementos.getModulos();
    //     foreach (JSONNode modulo in data)
    //     {
    //         Debug.Log("modulo "+ modulo["nombre"]);
    //     }
    // }


    public IEnumerator getPreguntas()
    {
        UnityWebRequest www = null;
        WebClient client = new WebClient();
        bool conn = false;

        try
        {
            using (client.OpenRead("http://google.com/generate_204"))
            {
            }

            Debug.Log("Se estableció conexión con la red local");
            www = UnityWebRequest.Get(BASE_URL + "/api/public/preguntas");
            conn = true;
        }
        catch
        {
            Debug.Log("No está conectado a la red");
        }

        if (!conn || www == null) yield break;

        yield return www.SendWebRequest();

        JSONNode data;

        string path = "./file/noConnection/";
        Directory.CreateDirectory(path);
        string pathPreguntas = path + "listaPreguntas.txt";

        data = JSON.Parse(www.downloadHandler.text);

        File.WriteAllText(pathPreguntas, www.downloadHandler.text);

        foreach (JSONNode pregunta in data)
        {
            if (!pregunta["tieneImagen"]) continue;

            string pathImagenOpcion = "./file/noConnection/imagenesOpciones/pregunta-id-" + pregunta["id"] +
                                      "/";

            Directory.CreateDirectory(pathImagenOpcion);

            foreach (JSONNode opciones in pregunta["opciones"])
            {
                try
                {
                    string pathTemporal = pathImagenOpcion + opciones["idSecuencial"];
                    pathTemporal = pathTemporal + "-imagen.bin";
                    byte[] dataImg = client.DownloadData(opciones["urlImgOpcion"]);
                    File.WriteAllBytes(pathTemporal, dataImg);
                }
                catch (Exception e)
                {
                    Debug.Log("Error al descargar foto " + e.Message);
                }
            }
        }
    }

    private IEnumerator getModulos()
    {
        UnityWebRequest www1 = null;
        WebClient client = new WebClient();
        bool conn = false;

        try
        {
            using (client.OpenRead("http://google.com/generate_204")) ;
            //Debug.Log("Se estableció conexión con la red local");
            www1 = UnityWebRequest.Get(BASE_URL + "/api/public/modulos");
            conn = true;
        }
        catch
        {
            Debug.Log("No está conectado a la red");
        }

        if (!conn || www1 == null) yield break;
        yield return www1.SendWebRequest();

        var path = "./file/noConnection/";
        Directory.CreateDirectory(path);
        var pathModulos = path + "listaModulos.txt";

        File.WriteAllText(pathModulos, www1.downloadHandler.text);
        //Debug.Log("Modulos:   "+www1.downloadHandler.text);
    }

    public IEnumerator getUnidades()
    {
        UnityWebRequest www2 = null;
        WebClient client = new WebClient();
        bool conn = false;

        try
        {
            using (client.OpenRead("http://google.com/generate_204")) ;
            //Debug.Log("Se estableció conexión con la red local");
            www2 = UnityWebRequest.Get(BASE_URL + "/api/public/unidades");
            conn = true;
        }
        catch
        {
            Debug.Log("No está conectado a la red");
        }

        if (!conn || www2 == null) yield break;
        yield return www2.SendWebRequest();

        var path = "./file/noConnection/";
        Directory.CreateDirectory(path);
        var pathUnidades = path + "listaUnidades.txt";

        File.WriteAllText(pathUnidades, www2.downloadHandler.text);
        //Debug.Log("unidades:   "+www2.downloadHandler.text);
    }

    private string BASE_URL = "https://unity.jedai.group/server";
    // private string BASE_URL = "http://localhost:8080";
}