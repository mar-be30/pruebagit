﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlAnimBoton : MonoBehaviour
{
    public GameObject panelModulo;
    public GameObject panelActivid;

    private Animator animVagonControl;
   
    // Start is called before the first frame update
    void Start()
    {
        panelModulo.SetActive(true);
        panelActivid.SetActive(false); 
        animVagonControl = GetComponent<Animator>();
       // anim= GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reproducirAnimBot()
    {
        animVagonControl.Play("AnimVagon1");
        //anim.Play("AnimVagon1");
    }

    public void botonSigEscenaSonido()
    {
        reproducirAnimBot();
        Invoke("bttnActivUno", 2);
    }

     public void bttnActivUno()
    {
        // panelModulo.SetActive(false); 
        panelActivid.SetActive(true);
    }

    public void bttnCerrarPanActivUno()
    {
        //panelModulo.SetActive(true);
        panelActivid.SetActive(false);
    }
}
