﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.IO;

public class ControlResJuego2 : MonoBehaviour
{
    public Toggle opcion1;
    public Toggle opcion2;
    public Toggle opcion3;


    public GameObject panelCorrecJD;
    public GameObject panelIncorrecJD;
    public GameObject imagen1;
    public GameObject imagen2;
    public GameObject imagen3;
    public GameObject moduloPregunta;

    public Text valorVerdad1;

    public Text titulo;
    public Text subtitulo;

    private bool resCorrect = false;

    // Start is called before the first frame update
    void Start()
    {
        opcion1.isOn = false;
        opcion2.isOn = false;
        opcion3.isOn = false;
        panelCorrecJD.SetActive(false);
        panelIncorrecJD.SetActive(false);
        resCorrect = false;
        JSONNode data = CargarElementos.getPreguntas();
        foreach (JSONNode pre in data)
        {
            if (pre["id"] == moduloPregunta.GetComponent<DatosPregunta>().idPregunta)
            {
                titulo.text = pre["descripcion"];
                subtitulo.text = pre["nombre"];
                string[,] pathImagenes = new string[3, 2]; //se define como 3 por que solo son 3 imagenes

                // if(pre["multi"] == true){ //para cuando las opciones sean verdaderas mas de una

                // }
                foreach (JSONNode opciones in pre["opciones"])
                {
                    if (opciones["valida"] == true)
                    {
                        pathImagenes[0, 0] = "./file/noConnection/imagenesOpciones/pregunta-id-" + pre["id"] +
                                             "/" + opciones["idSecuencial"] + "-imagen.bin";
                        pathImagenes[0, 1] = "verdadero";
                    }
                }

                int i = 1;
                foreach (JSONNode opciones in pre["opciones"])
                {
                    if (i < 3 && opciones["valida"] == false)
                    {
                        pathImagenes[i, 0] = "./file/noConnection/imagenesOpciones/pregunta-id-" + pre["id"] +
                                             "/" + opciones["idSecuencial"] + "-imagen.bin";
                        pathImagenes[i, 1] = "falso";
                        i++;
                    }
                }

                //Debug.Log("Tamano ::: "+ pathImagenes.Length);
                for (int t = 0; t < (pathImagenes.Length / 2); t++)
                {
                    string tmp = pathImagenes[t, 0];
                    string tmpValor = pathImagenes[t, 1];
                    int r = Random.Range(0, (pathImagenes.Length / 2) - 1);
                    pathImagenes[t, 0] = pathImagenes[r, 0];
                    pathImagenes[t, 1] = pathImagenes[r, 1];
                    pathImagenes[r, 0] = tmp;
                    pathImagenes[r, 1] = tmpValor;
                }

                byte[] byteArray = File.ReadAllBytes(pathImagenes[0, 0]);
                Texture2D sampleTexture1 = new Texture2D(2, 2);
                bool isLoaded = sampleTexture1.LoadImage(byteArray);
                if (isLoaded)
                {
                    imagen1.GetComponent<RawImage>().texture = sampleTexture1;
                }

                valorVerdad1.text = pathImagenes[0, 1] + "," + pathImagenes[1, 1] + "," + pathImagenes[2, 1];
                //Debug.Log("validos ::: "+ pathImagenes[0, 1]+" "+pathImagenes[1, 1]+" "+pathImagenes[2, 1]);
                byteArray = File.ReadAllBytes(pathImagenes[1, 0]);
                Texture2D sampleTexture2 = new Texture2D(2, 2);
                isLoaded = sampleTexture2.LoadImage(byteArray);
                if (isLoaded)
                {
                    imagen2.GetComponent<RawImage>().texture = sampleTexture2;
                }


                byteArray = File.ReadAllBytes(pathImagenes[2, 0]);
                Texture2D sampleTexture3 = new Texture2D(2, 2);
                isLoaded = sampleTexture3.LoadImage(byteArray);
                if (isLoaded)
                {
                    imagen3.GetComponent<RawImage>().texture = sampleTexture3;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void validar(Text valor)
    {
        string textoValidacionAux = valor.text;
        char delimitador = ',';
        string[] valores = textoValidacionAux.Split(delimitador);
        if (opcion1.isOn == true && valores[0].Equals("verdadero"))
        {
            panelCorrecJD.SetActive(true);
        }
        else if (opcion2.isOn == true && valores[1].Equals("verdadero"))
        {
            panelCorrecJD.SetActive(true);
        }
        else if (opcion3.isOn == true && valores[2].Equals("verdadero"))
        {
            panelCorrecJD.SetActive(true);
        }
        else
        {
            panelIncorrecJD.SetActive(true);
        }
    }

    public void resCorrecJD()
    {
        resCorrect = true;
    }

    public void resIncorrecJD()
    {
        resCorrect = false;
    }

    public void validarResJD()
    {
        if (resCorrect == true)
        {
            panelCorrecJD.SetActive(true);
        }
        else
        {
            panelIncorrecJD.SetActive(true);
        }
    }

    public void bttnCerrarPregunta()
    {
        panelCorrecJD.SetActive(false);
        panelIncorrecJD.SetActive(false);
    }
}