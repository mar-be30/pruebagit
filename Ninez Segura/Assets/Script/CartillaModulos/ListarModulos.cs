﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ListarModulos : MonoBehaviour
{
    public Text modulo1Txt;
    public Text modulo2Txt;
    public Text modulo3Txt;
    public GameObject modulo;

    private Dictionary<string, JSONNode> modulos = new Dictionary<string, JSONNode>();
    private string id2;
    private string id3;

    public GameObject tripElegidoPrefab;

    // Start is called before the first frame update
    void Start()
    {
        JSONNode data = CargarElementos.getModulos();

        var categoria = tripElegidoPrefab.GetComponent<DatosTripulanteElegido>().categoria;

        foreach (JSONNode mod in data)
        {
            if (mod["categoria"] == categoria)
            {
                var codigo = mod["codigo"].Value;

                modulos[codigo] = mod;

                switch (codigo)
                {
                    case "1":
                        modulo1Txt.text = mod["nombre"];
                        break;
                    case "2":
                        modulo2Txt.text = mod["nombre"];
                        id2 = mod["id"];
                        break;
                    default:
                        modulo3Txt.text = mod["nombre"];
                        id3 = mod["id"];
                        break;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void openEscenModulo(string cod)
    {
        SceneManager.LoadScene("Modulos");

        JSONNode data = CargarElementos.getModulos();

        var mod = modulos[cod];

        modulo.GetComponent<DatosModuloElegido>().nombreModulo = mod["nombre"];
        modulo.GetComponent<DatosModuloElegido>().idModulo = mod["id"];
        modulo.GetComponent<DatosModuloElegido>().mision = mod["mision"];
        
        SceneManager.LoadScene("Modulos");
    }
}