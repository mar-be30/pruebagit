﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text;
using System.Net;
using SimpleJSON;
using System.Threading;
using System.Threading.Tasks;
using SimpleJSON;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class VerificarRespCorrecta : MonoBehaviour
{
    public ControlSoltar respueta;
    public GameObject panlCorrecto;
    public GameObject panlIncorrecto;
    public GameObject image;
    public GameObject image1;
    public GameObject image2;
    public GameObject image3;
    public Texture2D target;
    public JSONNode data;
    public GameObject moduloPregunta;

    public JSONNode dataOpciones;
    public JSONNode preguntaSel;
    public Text textoTitulo;
    public Text textoSubTitulo;
    public string[] todo;
    public string[] incorrectas;

    public string[] correctas;

    void Start()
    {
        panlCorrecto.SetActive(false);
        panlIncorrecto.SetActive(false);


        data = CargarElementos.getPreguntas();


        int i = 0;
        //Debug.Log((modulo.GetComponent<DatosModuloElegido>().idModulo).text);
        foreach (JSONNode pregunta in data)
        {
            // if (pregunta["id"] == moduloPregunta.GetComponent<DatosPregunta>().idPregunta && pregunta["idModulo"] == modulo.GetComponent<DatosModuloElegido>().idModulo && pregunta["idUnidad"] == moduloUnidad.GetComponent<DatosUnidad>().idUnidad)
            if (pregunta["id"] == moduloPregunta.GetComponent<DatosPregunta>().idPregunta)

            {
                // moduloPregunta.GetComponent<DatosPregunta>().idPregunta = pregunta["id"];
                i = pregunta["opciones"].Count;
                dataOpciones = pregunta["opciones"];
                preguntaSel = pregunta;
            }
        }


        textoTitulo.text = preguntaSel["nombre"];

        textoSubTitulo.text = preguntaSel["descripcion"];

        var limiteOpciones = 3; // todo from pregunta objeto

        if (i > 3)
        {
            todo = new string[limiteOpciones];
        }
        else
        {
            todo = new string[i];
        }

        var j = 0;
        var k = 0;


        foreach (JSONNode opciones in dataOpciones)
        {
            if (opciones["valida"] == true)
            {
                j++;
            }
            else
            {
                k++;
            }
        }

        correctas = new string[j];
        incorrectas = new string[k];

        j = 0;
        k = 0;
        var m = 0;

        foreach (JSONNode opciones in dataOpciones)
        {
            if (opciones["valida"] == true)
            {
                correctas[j] = opciones["idSecuencial"];
                j++;
                m++;
            }
            else
            {
                incorrectas[k] = opciones["idSecuencial"];
                k++;
                m++;
            }
        }

        for (int t = 0; t < incorrectas.Length; t++)
        {
            string tmp = incorrectas[t];
            int r = Random.Range(t, incorrectas.Length - 1);
            incorrectas[t] = incorrectas[r];
            incorrectas[r] = tmp;
        }

        int d = 0;
        int e = 0;

        for (int t = j; t < todo.Length; t++)
        {
            if (t == j)
            {
                for (int p = 0; p < correctas.Length; p++)
                {
                    todo[p] = correctas[e];
                    e++;
                }

                todo[t] = incorrectas[d];
                d++;
            }
            else
            {
                todo[t] = incorrectas[d];
                d++;
            }
        }


        for (int t = 0; t < todo.Length; t++)
        {
            string tmp = todo[t];
            int r = Random.Range(t, todo.Length - 1);
            todo[t] = todo[r];
            todo[r] = tmp;
        }

        //string pathImagenOpcion = "./file/noConnection/imagenesOpciones/pregunta-id-5fc665a422c1120bf5d672f9" + "/opcion-id-1/imagen.bin";

        /* byte[] imageData = File.ReadAllBytes(pathImagenOpcion);

         target = new Texture2D(152, 152);
         target.LoadImage(imageData);
         bool isLoaded = target.LoadImage(imageData);
         if (isLoaded)
         {
             Debug.Log("Entro");
             image1.GetComponent<RawImage>().texture = target;
         }
        */


        string pathImagenOpcion = "./file/noConnection/imagenesOpciones/pregunta-id-" + preguntaSel["id"] +
                                  "/";

        for (int t = 0; t < todo.Length; t++)
        {
            string pathTemporal = pathImagenOpcion + todo[t];

            pathTemporal = pathTemporal + "-imagen.bin";

            byte[] imageData = File.ReadAllBytes(pathTemporal);

            target = new Texture2D(152, 152);
            target.LoadImage(imageData);
            bool isLoaded = target.LoadImage(imageData);
            if (isLoaded)
            {
                switch (t)
                {
                    case 0:
                        Debug.Log("Entro");
                        image1.GetComponent<RawImage>().texture = target;
                        image1.name = todo[t];
                        break;
                    case 1:
                        Debug.Log("Entro");
                        image2.GetComponent<RawImage>().texture = target;

                        image2.name = todo[t];
                        break;
                    case 2:
                        Debug.Log("Entro");
                        image3.GetComponent<RawImage>().texture = target;

                        image3.name = todo[t];
                        break;
                    case 3:
                        Debug.Log("Entro");
                        // image4.GetComponent<RawImage>().texture = target;
                        break;
                }
            }
        }
    }

    public void respCorrecta()
    {
        int cont = 0;
        for (int i = 0; i < correctas.Length; i++)
        {
            if (correctas[i] == respueta.item.name)
            {
                cont++;
            }
        }

        if (cont == 1)
        {
            Debug.Log("Esta bien");
            panlCorrecto.SetActive(true);
        }
        else
        {
            Debug.Log("Esta mal");
            panlIncorrecto.SetActive(true);
        }
    }
}