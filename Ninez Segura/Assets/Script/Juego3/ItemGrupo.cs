﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemGrupo : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        if (ControlArrastre.objBeingDraged == null) return;
        ControlArrastre.objBeingDraged.transform.SetParent(transform);
    }

}
